var webStorage = new WebStorageFactory().getWebStorage();
var spriteRanges={0:{tl:{x:0,y:0},br:{x:0,y:0}},1:{tl:{x:1,y:0},br:{x:1,y:0}},2:{tl:{x:2,y:1},br:{x:2,y:1}},3:{tl:{x:4,y:2},br:{x:4,y:2}},4:{tl:{x:9,y:5},br:{x:9,y:5}},5:{tl:{x:18,y:10},br:{x:18,y:10}},6:{tl:{x:36,y:20},br:{x:36,y:20}},7:{tl:{x:73,y:41},br:{x:73,y:41}},8:{tl:{x:147,y:82},br:{x:147,y:82}},9:{tl:{x:294,y:164},br:{x:295,y:164}},10:{tl:{x:589,y:328},br:{x:591,y:329}},11:{tl:{x:1179,y:657},br:{x:1182,y:659}},12:{tl:{x:2358,y:1315},br:{x:2364,y:1319}},13:{tl:{x:4717,y:2630},br:{x:4728,y:2639}},14:{tl:{x:9439,y:5262},br:{x:9452,y:5277}},15:{tl:{x:18878,y:10525},br:{x:18905,y:10554}},16:{tl:{x:37757,y:21051},br:{x:37810,y:21108}}};
var infowindow =  new google.maps.InfoWindow();
var bounds = new google.maps.LatLngBounds();
var max_zoom = 15;
var minZoomLevel = 9;
var imgSprites = [];
var mapTypeIds = [];
var markers = [];
var properties = [];

var latitude = 10.784070141719273;
var longitude = 122.58819580078125;

var directionsService = new google.maps.DirectionsService;
var directionsDisplay = new google.maps.DirectionsRenderer;

Ti.App.addEventListener("app:fromTitanium", function(e) {
    latitude = e.latitude;
    longitude = e.longitude;
    properties = e.data;
    init(latitude,longitude);
});

jQuery('#map').width( jQuery(window).width() );
jQuery('#map').height( jQuery(window).height() );

jQuery(document).ready(function(){
	Ti.App.fireEvent('app:fromWebView', { message: 'Load Data' });
	//alert('WIDTH: '+width+' HEIGHT: '+height);
});

function openDetails( index ){
	Ti.App.fireEvent('app:openDetails', { data: properties[index] });
}

function showRoute( lat, lng ) {
	directionsService.route({
  		origin: {lat: latitude, lng: longitude},
  		destination: {lat: lat, lng: lng},
  		travelMode: 'DRIVING'
	}, function(response, status) {
  		if (status === 'OK') {
    		directionsDisplay.setDirections(response);
  		} else {
    		alert('Failed due to ' + status + ' or check your internet connection.');
      	}
    });
}

function init(latitude,longitude)
{

    for(var type in google.maps.MapTypeId) { 
        mapTypeIds.push(google.maps.MapTypeId[type]); 
    }

    mapTypeIds.push("OpenSourceMap");
    mapTypeIds.push("LocalGmap");
    mapTypeIds.push("WebStorageGmap");
    mapTypeIds.push("LocalMyGmap");
    mapTypeIds.push("WebStorageMyGmap");

    var map = new google.maps.Map( document.getElementById("map"), {
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 10,
        //minZoom: 3,
        //maxZoom: 15,
        mapTypeId: "OpenSourceMap",
        zoomControl: false,
        mapTypeControlOptions: {
            mapTypeIds: mapTypeIds,
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
        }
    });
	
   	directionsDisplay.setMap(map);
   	
    var gps = new google.maps.Marker({
        position: new google.maps.LatLng( latitude, longitude ),
        title: 'Current Location',
        map: map,
        icon: 'redpin.png'
    });
	
    markers.push( gps );

    for (var i in properties ) {

        markers[i] = new google.maps.Marker({
            position: new google.maps.LatLng( properties[i].latitude, properties[i].longitude ),
            title: properties[i].name,
            map: map,
            icon: 'bluepin.png'
        });

        bindInfoWindow( markers[i], map, infowindow, properties[i], i );
    }
	
    bounds.extend( new google.maps.LatLng( 10.685236942964098, 122.50133514404297 ) );
    bounds.extend( new google.maps.LatLng( 10.670729697573023, 122.54905700683594 ) );
    bounds.extend( new google.maps.LatLng( 10.720827362865535, 122.60673522949219 ) );
    bounds.extend( new google.maps.LatLng( 10.73516355399347, 122.55420684814453 ) );
	map.fitBounds(bounds);
	
    google.maps.event.addListener(map, 'dragend', function() { 
        map.fitBounds(bounds);
    });

    map.mapTypes.set("OpenSourceMap", new google.maps.ImageMapType({
        getTileUrl: getOsmTileImgSrc,
        tileSize: new google.maps.Size(256, 256),
        name: "OpenSourceMap",
        maxZoom: 15,
        minZoom: 3,
    }));

    map.mapTypes.set("LocalGmap", new google.maps.ImageMapType({
        getTileUrl: getLocalTileImgSrc,
        tileSize: new google.maps.Size(256, 256),
        name: "LocalGmap",
        zoomControl: false,
        minZoom: 9,
        maxZoom: 15
    }));

    map.mapTypes.set("WebStorageGmap", new google.maps.ImageMapType({
        getTileUrl: getWebStorageTileImgSrc,
        tileSize: new google.maps.Size(256, 256),
        name: "WebStorageGmap",
        minZoom: 9,
        maxZoom: 15
    }));

    map.mapTypes.set("LocalMyGmap", new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
            return checkTileInSprites(coord, zoom) ? getLocalTileImgSrc(coord, zoom) : getGmapTileImgSrc(coord, zoom);
        },
        tileSize: new google.maps.Size(256, 256),
        name: "LocalMyGmap",
        minZoom: 9,
        maxZoom: 15
    }));

    map.mapTypes.set("WebStorageMyGmap", new google.maps.ImageMapType({
        getTileUrl: function(coord, zoom) {
            var image = getWebStorageTileImgSrc(coord, zoom);
            return image ? image :  getGmapTileImgSrc(coord, zoom);
        },
        tileSize: new google.maps.Size(256, 256),
        name: "WebStorageMyGmap",
        minZoom: 9,
        maxZoom: 15
    }));

    var clearWebStorageDiv = document.createElement('DIV');
    var clearWebStorageButton = new CustomControl(clearWebStorageDiv, map,
        'Clear Web Storage',  clearWebStorage);

    var prepareWebStorageDiv = document.createElement('DIV');
    var prepareWebStorageButton = new CustomControl(prepareWebStorageDiv, map,
        'Prepare Web Storage', prepareWebStorage);

    clearWebStorageDiv.index = 1;
    prepareWebStorageDiv.index = 1;
}

function bindInfoWindow(marker, map, infowindow, data, index) {
    var desc = data.description.substring(0, 120);
    var btn = '<button onClick="showRoute( '+data.latitude+', '+data.longitude+' )" style="display:inline-block;">Show Route</button>';
    google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent('<table width="300px"><tr><td class="inf"><b>'+data.name+'</b><br><span>'+desc+'...</span><div style="margin:10px 0 0 0;"><button style="display:inline-block;margin-right:10px" onClick="openDetails('+index+')">View Details</button>'+btn+'</div></td></tr></table>');
        infowindow.open(map, marker);
    });
}

function CustomControl(controlDiv, map, title, handler) {
    controlDiv.style.padding = '5px';

    var controlUI = document.createElement('DIV');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '2px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'center';
    controlUI.title = title;
    controlDiv.appendChild(controlUI);

    var controlText = document.createElement('DIV');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = title;
    controlUI.appendChild(controlText);

    google.maps.event.addDomListener(controlUI, 'click', handler);
}

function LocalStorageWebStorageImpl() {
    this.webStorageType = 'localStorage';

    this.getItem = function(name) {
        return localStorage.getItem(name);
    };

    this.setItem = function(name, value) {
        localStorage.setItem(name, value);
    };

    this.clear = function() {
        localStorage.clear();
    };
}

function WebStorageFactory() {
    var webStorage = null;

    if (window.localStorage) {
        webStorage = new LocalStorageWebStorageImpl();
    } else {
        alert("Your browser don't support localStorage");
    }

    this.getWebStorage = function() {
        return webStorage;
    };
}


function imageToBase64(image) {
    var canvas = document.createElement("canvas");
    canvas.width = image.width;
    canvas.height = image.height;

    var context = canvas.getContext("2d");
    context.drawImage(image, 0, 0);

    return canvas.toDataURL("image/png");
}

function loadImageToWebStorage(zoom, x, y){
    var url =  "img/" + zoom + "/" + x + "_" + y + ".png";
    var image = new Image();
    image.onload = function() {
        webStorage.setItem([zoom, x, y].join('_'), imageToBase64(image));
    };
    image.src = url;
}

function clearWebStorage() {
    webStorage.clear();
}

function prepareWebStorage() {
    for (var zoom in spriteRanges) {
        if (zoom > max_zoom) { break; }
        var sprites = spriteRanges[zoom];
        for (var x=sprites.tl.x; x<=sprites.br.x; x++) {
            for (var y=sprites.tl.y; y<=sprites.br.y; y++) {
                loadImageToWebStorage(zoom, x, y);
                //imgSprites.push( { zoom: zoom, x: x, y: y } );
            }
        }
    }
}

function checkTileInSprites(coord, zoom) {
    var sprites = spriteRanges[zoom];
    return sprites.tl.x <= coord.x && coord.x <= sprites.br.x && sprites.tl.y <= coord.y && coord.y <= sprites.br.y;
}

function getOsmTileImgSrc(coord, zoom) {
    var url = "http://tile.openstreetmap.org/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    return url;
}

function getGmapTileImgSrc(coord, zoom) {
    var url = "http://mt0.googleapis.com/vt?src=apiv3&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    return url;
}

function getLocalTileImgSrc(coord, zoom) {
    var url = "img/" + zoom + "/" + coord.x + "_" + coord.y + ".png";

    return url;
}

function getWebStorageTileImgSrc(coord, zoom) {
    return webStorage.getItem([zoom, coord.x, coord.y].join('_'));
}
// Arguments passed into this controller can be accessed via the `$.args` object directly or:
//var args = $.args;
var args = arguments[0] || {};
var landmark = args.landmark;

var loadingWrap = Ti.UI.createView({
	layout: 'vertical',
	height: Ti.UI.SIZE,
	width: "90%",
	visible: true
});

var activityIndicator = Ti.UI.createActivityIndicator({
	color: '#fff',
	font: {fontFamily:'Helvetica Neue', fontSize:12,},
	message: ' Loading...',
	style: Ti.UI.ActivityIndicatorStyle.DARK,
	top: 150,
	left: 10,
	height: Ti.UI.SIZE,
	width:'100%'
});

activityIndicator.show();
loadingWrap.add( activityIndicator );
$.scrollView.add( loadingWrap );

if ( Titanium.Network.online == true ) {
		
	var url = Alloy.Globals.serverUrl+'api/gallery/'+landmark.gallery_id;
	var xhr = Ti.Network.createHTTPClient({
		
	    onload: function( e ) {
	    	var response = JSON.parse( this.responseText );
	    	
	    	if( response.error==false && response.data.length ){
	    		
				var url = encodeURI(Alloy.Globals.serverUrl+'uploads/'+response.data[0].file_name);
				
				var imageView = Ti.UI.createImageView({
					preventDefaultImage:true,
				    image: url,
				    height: Ti.UI.SIZE,
				    width: Ti.UI.FILL,
				    left: 0,
				    top: 0,
				});
				
				$.scrollView.add( imageView );
				
	    	}
			    	
	    	afterload();
	
	    },
	    onerror: function( e ) {
	    	var newtoast = Ti.UI.createNotification({
			    message:"Error Pulling Data.",
			    duration: Ti.UI.NOTIFICATION_DURATION_LONG
			});
			
			newtoast.show();
			$.detailsWindow.close();
	    },
	    timeout:40000  /* in milliseconds */ 
	});
	
	xhr.open( "GET", url );
	xhr.send();
	
} else {
	
	afterload();
}

function afterload()
{
	var title = Ti.UI.createLabel({
		text: landmark.name,
		color: '#fff',
		font:{
			fontWeight:'bold',
			fontSize: 20
		},
		width: '95%',
		height: Ti.UI.SIZE,
		left: 10,
		top: 10,
	});
	
	$.scrollView.add( title );
	
	if (landmark.address) {
		var address = Ti.UI.createLabel({
			text: landmark.address,
			color: '#b6b6b6',
			font:{fontSize: 10},
			width: '95%',
			height: Ti.UI.SIZE,
			top: 10,
			left: 10,
		});
		
		$.scrollView.add( address );
	};
	
	if ( landmark.phone_number ) {
		
		var phone_number = Ti.UI.createLabel({
			text: 'Phone: '+landmark.phone_number,
			color: '#b6b6b6',
			font:{fontSize: 10},
			width: '95%',
			height: Ti.UI.SIZE,
			top: 10,
			left: 10,
		});
		
		$.scrollView.add( phone_number );
	};
	
	if ( landmark.mobile_number ) {
		
		var mobile_number = Ti.UI.createLabel({
			text: 'Mobile: '+landmark.mobile_number,
			color: '#b6b6b6',
			font:{fontSize: 10},
			width: '95%',
			height: Ti.UI.SIZE,
			top: 10,
			left: 10,
		});
		
		$.scrollView.add( mobile_number );
	};
	
	if ( landmark.email ) {
		
		var email = Ti.UI.createLabel({
			text: 'Email: '+landmark.email,
			color: '#b6b6b6',
			font:{fontSize: 10},
			width: '95%',
			height: Ti.UI.SIZE,
			top: 10,
			left: 10,
		});
		
		$.scrollView.add( email );
	};
	
	if ( landmark.description ) {
		
		var details = Ti.UI.createLabel({
			backgroundColor: '#fd930c',
			text: '  DETAILS',
			color: '#fff',
			font:{
				fontWeight:'bold',
				fontSize: 15
			},
			left: 0,
			top: 10,
			width: '100%',
			height: 25
		});
		
		$.scrollView.add( details );
		
		var description = Ti.UI.createLabel({
			text: landmark.description,
			color: '#fff',
			font:{
				fontSize: 13
			},
			width: '95%',
			top: 10,
		});
		
		$.scrollView.add( description );
	};
	
	if ( landmark.transportation ) {
		
		var transportation = Ti.UI.createLabel({
			backgroundColor: '#fd930c',
			text: '  TRANSPORTATION',
			color: '#fff',
			font:{
				fontWeight:'bold',
				fontSize: 15
			},
			left: 0,
			top: 10,
			width: '100%',
			height: 25
		});
		
		var transpo_description = Ti.UI.createLabel({
			text: landmark.transportation,
			color: '#fff',
			font:{
				fontSize: 13
			},
			width: '95%',
			top: 10,
		});
		
		$.scrollView.add( transportation );
		$.scrollView.add( transpo_description );
	}
	
	/*
	var mapsbtn = Ti.UI.createButton({
		title: 'View MAP',
		height: Ti.UI.SIZE,
		width: Ti.UI.SIZE,
		top: 10
	});
	*/
	// $.scrollView.add( mapsbtn );	
	/*
	mapsbtn.addEventListener('click',function(e){
		var mapsWindow = Alloy.createController('route',{data:args.landmark,home:args.home}).getView();
		args.home.open( mapsWindow );
	});
	*/
	

	
	var bottompadding = Ti.UI.createView({
		layout: 'horizontal',
		width: Ti.UI.SIZE,
		height: 20,
		backgroundColor: "#1d1d1d",
	});
	
	$.scrollView.add( bottompadding );
	
	activityIndicator.hide(); 
	loadingWrap.applyProperties({height:1,visible:false});
}




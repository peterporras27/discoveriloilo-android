// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var landmarks = require('landmarks');

Ti.Geolocation.purpose = "Receive User Location";
Ti.Geolocation.getCurrentPosition(function(e){
    if (!e.success || e.error){ 
    	alert( e.error );
    } else {
    	Alloy.Globals.longitude = e.coords.longitude;
    	Alloy.Globals.latitude = e.coords.latitude;
    	
    	console.log(' ');
    	console.log('MAP.JS ');
    	console.log(' ');
    	console.log('longitude: '+e.coords.longitude);
    	console.log('latitude: '+e.coords.latitude);
    	console.log(' ');
    	
    }
});

Ti.App.addEventListener('app:fromWebView', function() {
	Ti.App.fireEvent('app:fromTitanium', { 
		longitude: Alloy.Globals.longitude,
		latitude: Alloy.Globals.latitude, 
		data: landmarks.lists
	});
});

console.log(landmarks.lists);

var openDetails = function(e) {
	var detailsWindow = Alloy.createController('details',{landmark:e.data,home:args.home}).getView();
	args.home.open( detailsWindow );
};

Ti.App.addEventListener('app:openDetails', openDetails);

$.mapsWindow.addEventListener('close', function() {
	// to remove an event listener, you must use the exact same function signature
	// as when the listener was added
	Ti.App.removeEventListener('app:openDetails', openDetails);
});


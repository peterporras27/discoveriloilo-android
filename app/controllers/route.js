// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = arguments[0] || {};
var landmarks = require('landmarks');
var landmark = args.data;
var Map = require('ti.map');
var annotations = [];
var latitude = 11.151455875525711;
var longitude = 122.431640625;
var toast = Ti.UI.createNotification({
    message:"Loading Data...",
    duration: Ti.UI.NOTIFICATION_DURATION_LONG
});
		
var pin = Ti.Map.createAnnotation({
	latitude: landmark.latitude.replace(/[^\d.]+/g, ''),
	longitude: landmark.longitude.replace(/[^\d.]+/g, ''),
	title: landmark.name,
	subtitle: landmark.address,
	animate: true,
	image: '/landmarks/pin.png',
});
	
annotations.push( pin );

var mapview = Ti.Map.createView({
    mapType: Ti.Map.STANDARD_TYPE,
    region: {
    	latitude: 11.151455875525711,
    	longitude: 122.431640625,
        latitudeDelta: 1,
        longitudeDelta: 1
    },
    animate:true,
    regionFit:true,
    userLocation:true,
    annotations: annotations,
    userLocation:true
});

mapview.addEventListener('click', function(evt) {
    if (evt.clicksource=='pin') {
    	setRoute(evt.latitude,evt.longitude);	
    };
});

Ti.Geolocation.purpose = "Receive User Location";
Titanium.Geolocation.getCurrentPosition(function(e){
    
    if (!e.success || e.error){return;}
    
    longitude = e.coords.longitude;
    latitude = e.coords.latitude;
	
	setRoute(args.data.latitude,args.data.longitude);
});

$.routeWindow.add( mapview );

function setRoute(lat,lng)
{
	var mapurl = 'http://maps.googleapis.com/maps/api/directions/json?origin='+latitude+','+longitude+'&destination='+lat+','+lng+'&sensor=false';
	var xhr = Ti.Network.createHTTPClient();
    xhr.onload = function (e) {
        
        //*
        var response = this.responseText;
        var json = JSON.parse(response);
        Ti.API.info('onload', JSON.stringify(json));

        var step = [];
        if ( json.routes.length > 0 ) {
        	step = json.routes[0].legs[0].steps;	
        }
        
        var intStep = 0, intSteps = step.length, points = [];
        var decodedPolyline, intPoint = 0, intPoints = 0;
        
        for (intStep = 0; intStep < intSteps; intStep = intStep + 1) {
            decodedPolyline = decodeLine(step[intStep].polyline.points);
            intPoints = decodedPolyline.length;
            for (intPoint = 0; intPoint < intPoints; intPoint = intPoint + 1) {
                if (decodedPolyline[intPoint] != null) {
                    points.push({
                        latitude: decodedPolyline[intPoint][0],
                        longitude: decodedPolyline[intPoint][1]
                    });
                }
            }
        }
		
        var myroute = {
            name: 'Example Route',
            points: points,
            color: '#f00',
            width: 4
        };
        mapview.addRoute(myroute);    
        //*/
    };
    xhr.onerror = function (e) {
        Ti.API.info('error', JSON.stringify(e));
    };
    xhr.open('GET', mapurl);
    xhr.send();
}


function decodeLine(encoded) {
    var len = encoded.length;
    var index = 0;
    var array = [];
    var lat = 0;
    var lng = 0;

    while (index < len) {
        var b;
        var shift = 0;
        var result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);

        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;

        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);

        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;

        array.push([lat * 1e-5, lng * 1e-5]);
    }

    return array;
}


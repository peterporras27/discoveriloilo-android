// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = arguments[0] || {};
var landmarks = require('landmarks');
var toast = Ti.UI.createNotification({
    message:"Loading Data...",
    duration: Ti.UI.NOTIFICATION_DURATION_LONG
});

toast.show();

var dataTable = Ti.UI.createTableView({
	data: [],
	top: 0,
	zIndex: 10,
});

if ( Titanium.Network.online == true ) {
	
	pullData();	
	
} else {
	var cats = Ti.App.Properties.getObject('categories');
	
	if (cats) {	
		process( cats );
	};
}


$.categoryWindow.add( dataTable );

dataTable.addEventListener('click',function(e){
	args.formData.cat = e.row._data.id;
	args.formData.cat_name = e.row._data.name;
	$.categoryWindow.close();
});

function process( categories ){
	
	if ( landmarks.lenght<1 ){ return; }
	
	var lists = [];
	
	categories.unshift({name:'Select Category',id:''});
	
	_.each( categories, function( cat ) {
	
		var row = Ti.UI.createTableViewRow({
			height: Ti.UI.SIZE,
			color:'#fff',
			title: cat.name,
			backgroundColor: '#2c2c2c',
			_data: cat
		});	
		
		lists.push( row );
		
	});
	
	toast.hide();
	dataTable.setData( lists );
}

function pullData()
{
	var url = Alloy.Globals.serverUrl+'api/category';
	var xhr = Ti.Network.createHTTPClient({
	    onload: function( e ) {
	    	var response = JSON.parse( this.responseText );
	    	var landmarks = require('landmarks');
    		landmarks.categories = response.data;    	
    		Ti.App.Properties.setObject('categories', response.data);	
    		process( response.data );
	    },
	    onerror: function( e ) {
			console.log('error pulling data.');
	    },
	    timeout:40000  /* in milliseconds */ 
	});
	
	xhr.open( "GET", url );
	xhr.send();
}


// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var formData = {cat:'',key:'',cat_name:'Select Category'};
var mapsbtn = Ti.UI.createButton({
	title: 'MAP SEARCH',
	height: Ti.UI.SIZE,
	width: "90%",
	top: 10
});

var input = Ti.UI.createTextField({
	borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
	color: '#fff',
	top: 10,
	height: Ti.UI.SIZE,
	width: "90%",
	value: "Keyword",
	textAlign: "center"
});

var picker = Ti.UI.createButton({ 
	title: formData.cat_name,
	height: Ti.UI.SIZE,
	width: "90%",
	color: '#fff',
	top: 10
});

var search = Ti.UI.createButton({
	title: 'SEARCH',
	height: Ti.UI.SIZE,
	width: "90%",
	top: 10
});

var Window = Alloy.createController('category',{formData:formData,home:$.searchTab}).getView();

picker.addEventListener('click',function(e){
	$.searchTab.open( Window );
});

Window.addEventListener('close',function(e){
	picker.setTitle( formData.cat_name );
});

search.addEventListener('click',function(e){
	formData.key = input.value;
	if ( Titanium.Network.online == true ) {
		var resultWindow = Alloy.createController('result',{formData:formData,home:$.searchTab}).getView();
		$.searchTab.open( resultWindow );	
	} else {
		alert('Turn on your internet to enable search.');
	}
	
});

mapsbtn.addEventListener('click',function(e){
	var mapsWindow = Alloy.createController('map',{home:$.searchTab}).getView();
	$.searchTab.open( mapsWindow );
});

input.addEventListener('focus',function(e){
	this.value = "";
});

$.scrollView.add( mapsbtn );
$.scrollView.add( picker );
$.scrollView.add( input );
$.scrollView.add( search );

$.searchWindow.addEventListener('open',function(evt){
    input.blur();
});


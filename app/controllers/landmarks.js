// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args;
var landmarks = require('landmarks');

var dataTable = Ti.UI.createTableView({
	data: [],
	top: 0,
	zIndex: 10,
});

$.landmarkWindow.add( dataTable );

var savedDatas = Ti.App.Properties.getObject('landmarks');

if ( Titanium.Network.online == false ) {
	
	if ( savedDatas ){
		process( savedDatas );
	} else {
		process( landmarks.lists );	
	}
	
} else {
	
	process( landmarks.lists );

}

$.landmarkWindow.addEventListener('open', function(){
	process( landmarks.lists );
});

dataTable.addEventListener('click',function(e){
	var detailsWindow = Alloy.createController('details',{landmark:e.row._data,home:$.landmarkTab}).getView();
	$.landmarkTab.open(detailsWindow);
});

function process( landmarks ){
	
	if ( landmarks.lenght<1 ){ return; }
	
	var lists = [];
	
	_.each( landmarks, function( landmark ) {
	
		var row = Ti.UI.createTableViewRow({
			height: 70,
			color:'#2c2c2c',
			backgroundColor: '#2c2c2c',
			_data: landmark
		});	
		
		row.title = landmark.name;
		
		var title = Ti.UI.createLabel({
			text: landmark.name,
			color: '#fff',
			font:{
				fontWeight:'bold',
				fontSize: 17
			},
			top: 10,
			left: 10,
		});
		
		row.add( title );
		
		if ( landmark.address ) {
			
			var address = Ti.UI.createLabel({
				text: landmark.address,
				color: '#b6b6b6',
				font:{fontSize: 10},
				top: 40,
				left: 10,
			});
			
			row.add( address );
		};
		
		lists.push( row );
		
	});
	
	dataTable.setData( lists );

}


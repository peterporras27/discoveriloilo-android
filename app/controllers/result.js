// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = arguments[0] || {};

var landmarks = require('landmarks');
var toast = Ti.UI.createNotification({
    message:"Loading Data...",
    duration: Ti.UI.NOTIFICATION_DURATION_LONG
});

toast.show();

var activityIndicator = Ti.UI.createActivityIndicator({
	color: '#fff',
	font: {fontFamily:'Helvetica Neue', fontSize:12,},
	message: ' Loading...',
	style: Ti.UI.ActivityIndicatorStyle.DARK,
	top: 100,
	left: 10,
	height: Ti.UI.SIZE,
	width:'100%'
});

activityIndicator.show();
$.scrollView.add(activityIndicator);

var dataTable = Ti.UI.createTableView({
	data: [],
	top: 0,
	zIndex: 10,
});

pullData();	

$.resultWindow.add( dataTable );

dataTable.addEventListener('click',function(e){
	var detailsWindow = Alloy.createController('details',{landmark:e.row._data,home:args.home}).getView();
	args.home.open(detailsWindow);
});

function process( landmarks ){
	
	if ( landmarks.lenght<1 ){ return; }
	
	var lists = [];
	
	_.each( landmarks, function( landmark ) {
	
		var row = Ti.UI.createTableViewRow({
			height: 70,
			color:'#2c2c2c',
			backgroundColor: '#2c2c2c',
			_data: landmark
		});	
		
		row.title = landmark.name;
		
		var title = Ti.UI.createLabel({
			text: landmark.name,
			color: '#fff',
			font:{
				fontWeight:'bold',
				fontSize: 17
			},
			top: 10,
			left: 10,
		});
		
		row.add( title );
		
		if ( landmark.address ) {
			
			var address = Ti.UI.createLabel({
				text: landmark.address,
				color: '#b6b6b6',
				font:{fontSize: 10},
				top: 40,
				left: 10,
			});
			
			row.add( address );
		};
		
		lists.push( row );
		
	});
	
	activityIndicator.hide();
	toast.hide();
	dataTable.setData( lists );
	
}

function pullData()
{
	var url = Alloy.Globals.serverUrl+'api/search?key='+args.formData.key+'&cat='+args.formData.cat;
	var xhr = Ti.Network.createHTTPClient({
	    onload: function( e ) {
	    	var response = JSON.parse( this.responseText );
	    	
	    	if (response.error) {
	    		
	    		var toast = Ti.UI.createNotification({
				    message:"No Results Found.",
				    duration: Ti.UI.NOTIFICATION_DURATION_LONG
				});

				toast.show();
				
				setTimeout(function(){
					$.resultWindow.close();	
				}, 3000);
	    	};
	    	
    		process( response.data );
	    },
	    onerror: function( e ) {
			console.log('error pulling data.');
	    },
	    timeout:40000  /* in milliseconds */ 
	});
	
	xhr.open( "GET", url );
	xhr.send();
}



Ti.Geolocation.purpose = "Receive User Location";
Ti.Geolocation.getCurrentPosition(function(e){
    if (!e.success || e.error){ 
    	alert(e.error);
    } else {
    	Alloy.Globals.longitude = e.coords.longitude;
    	Alloy.Globals.latitude = e.coords.latitude;
    	
    	console.log(' ');
    	console.log('INDEX.JS');
    	console.log(' ');
    	console.log('longitude: '+e.coords.longitude);
    	console.log('latitude: '+e.coords.latitude);
    	console.log(' ');
    }
});

// check if internet is active
if ( Titanium.Network.online == true ) {
	
	var url = Alloy.Globals.serverUrl+'api/landmarks';
	var xhr = Ti.Network.createHTTPClient({
	    onload: function( e ) {
	    	
	    	var response = JSON.parse( this.responseText );
	    	var landmarks = require('landmarks');
    		
    		landmarks.setData( response.data );
    		landmarks.saveData( response.data );
    		landmarks.setCats( response.category );
			Ti.App.Properties.setObject('categories', response.category);
			
    		$.index.open();	
	    },
	    onerror: function( e ) {
	    	
	    	pullData();
	    	
			console.log('error pulling data.');
	    },
	    timeout:40000  /* in milliseconds */ 
	});
	
	xhr.open( "GET", url );
	xhr.send();
	
} else {
	
	var landmarks = require('landmarks');
	var datas = Ti.App.Properties.getObject('landmarks');
	var cats = Ti.App.Properties.getObject('categories');
	
	if ( cats ) {
		landmarks.setCats( cats );
	}	
	
	if ( datas ){
		
		landmarks.setData( datas );
		
		$.index.open();	
		
	} else {
		Alloy.createController('retry',{home:$.index,url:null}).getView().open();	
	}
}

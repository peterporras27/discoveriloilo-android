
var progresswrapper = Ti.UI.createView({
	layout: 'vertical',
	height: Ti.UI.SIZE,
	width: "90%",
	visible: true
});

var button = Ti.UI.createButton({
	title: 'Refresh',
	width: Ti.UI.SIZE,
	height: Ti.UI.SIZE,
	color: '#fff',
	visible: false
});

progresswrapper.add( button );

exports.showBtn = function(){
	this.button.applyProperties({ visible: true, height: Ti.UI.SIZE });
};

exports.hideBtn = function(){
	this.button.applyProperties({ visible: false, height: 1 });
};

exports.hideAll = function(){
	this.button.applyProperties({ visible: false, height: 1 });
	this.view.applyProperties({ visible: false, height: 1 });
};

exports.showAll = function(){
	this.button.applyProperties({ visible: true, height: Ti.UI.SIZE });
	this.view.applyProperties({ visible: true, height: Ti.UI.SIZE });
};

exports.clickOff = function(){
	this.button.removeEventListener('click',function(){});
};

exports.clickOn = function( listener ){
	this.button.removeEventListener('click',listener);
};

exports.button = button;
exports.view = progresswrapper;

// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};
Alloy.Globals.fullWidth = Ti.Platform.displayCaps.platformWidth;
Alloy.Globals.fullHeight = Ti.Platform.displayCaps.platformHeight;
Alloy.Globals.model = Ti.Platform.model;

Alloy.Globals.latittude = 10.784070141719273;
Alloy.Globals.longitude = 122.58819580078125;

Alloy.Globals.serverUrl = 'http://discoveriloilo.org/';
